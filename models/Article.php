<?php

class Article extends Model
{
    private static $selArticles = 'select * from articles';

    public static function findAll()
    {
        self::connect();
        $sql = self::$db->prepare(self::$selArticles);
        $sql->execute();
        $articles = $sql->fetchAll();

        return $articles;
    }

    public static function findOne($id)
    {
        self::connect();
        $sql = 'select * from articles where id = :id';
        $query = self::$db->prepare($sql);
        $query->bindValue(':id', (int) $id);
        $query->execute();
        $articles = $query->fetchObject();

        return $articles;
    }
}