<?php

class Post extends Model
{
    private static $selPosts = 'select * from posts';

    public static function findAll()
    {
        self::connect();
        $sql = self::$db->prepare(self::$selPosts);
        $sql->execute();
        $posts = $sql->fetchAll();

        return $posts;
    }

    public static function findOne($id)
    {
        self::connect();
        $sql = 'select * from posts where id = :id';
        $query = self::$db->prepare($sql);
        $query->bindValue(':id', (int) $id);
        $query->execute();
        $posts = $query->fetchObject();

        return $posts;
    }
}