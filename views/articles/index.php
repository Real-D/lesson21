<h1><?=$pageTitle?></h1>
<ul>
<?php foreach ($articles as $article) : ?>
    <li>
        <a href="/articles/view/<?=$article['id']?>"><strong><?=$article['title']?></strong></a>
    </li>
<?php endforeach ?>
</ul>
